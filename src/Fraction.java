
public class Fraction {

					private int num;
	                private int deno;
	                
	                Fraction(int num,int deno)
	                {
	                        this.setNum(num);
	                        this.setDeno(deno);
	                }
	        
	                public int getDeno() {
	                        return deno;
	                }
	        
	                public void setDeno(int deno) {
	                        this.deno = deno;
	                }
	        
	                public int getNum() {
	                        return num;
	                }
	        
	                public void setNum(int num) {
	                        this.num = num;
	                }
	                
	                public String toString()
	                {
	                        return this.num + "/" + this.deno;
	                }
	}


